import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HEROES } from '../mock-heroes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  addHero: Hero;
  constructor(private route: Router) { }
  addheroes = HEROES;
  ngOnInit() {
  }
    save(value: any) {
    let heroAdd: Hero = {
      id: this.addheroes.length + 1,
      name: value
    }
    this.addheroes.push(heroAdd);
    this.route.navigate(['/Vinhkm1']);
  }
}

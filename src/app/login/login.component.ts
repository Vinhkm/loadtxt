import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  inputAcc: string;
  showAlert: boolean = false;
  showAlert2: boolean = false;
  inputPass: string;
  constructor(private route: Router) {
  }

  ngOnInit() {
  }

  checkUserValidate() {
    if (this.inputAcc && this.inputAcc.length < 6) {
      this.showAlert = true;
      return true;
    } this.showAlert = false;
  }
  checkPassValidate() {
    if (this.inputPass && this.inputPass.length < 6) {
      this.showAlert2 = true;
      return true;
    } this.showAlert2 = false;
  }
  checkLogin() {
    if (this.checkUserValidate() || this.checkPassValidate()) {
      return;
    }
    this.route.navigate(['/Vinhkm1']);
  }

}

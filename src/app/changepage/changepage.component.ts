import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HEROES } from '../mock-heroes'
import { Router } from '@angular/router';
@Component({
  selector: 'app-changepage',
  templateUrl: './changepage.component.html',
  styleUrls: ['./changepage.component.css']
})
export class ChangepageComponent implements OnInit {

  heroes = HEROES;

  selectedHero: Hero;

  constructor(private route: Router) {
  }

  ngOnInit() {
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }
  onAdd(){
    this.route.navigate(['/add']);
  }
  onDelete() {
    this.heroes.splice(this.selectedHero.id - 1, 1);
  }
}
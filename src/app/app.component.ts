import { Component } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.onGetData();
  }
  onGetData() {
    this.http.get('./assets/1.txt').subscribe(val => {
      console.log(val);
    });
    // return this.http.get('https://jsonplaceholder.typicode.com/posts/1')
  }
}
